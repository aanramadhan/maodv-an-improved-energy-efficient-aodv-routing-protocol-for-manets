# maodv-An Improved Energy Efficient for MANETs 

Galuh Aan Ramadhan <br/>
05111540000026 <br/>
Kelas Jarnil 2018


<b> Penjelasan singkat modifikasi AODV </b><br/>
Paper berjudul <b> "An Improved Energy Efficient AODV Routing Protocol for MANETs"</b> mengintregasikan konsep drain count ke AODV untuk membuat energy effisien. Di packet header AODV RREQ,
ditambahkan field <b>Record</b>,<b>Drain Count</b>, dan <b> Remaining Energy </b>. Path yang sudah dilewati oleh RREQ akan disimpan di Record, Remaining Energy menyimpan info energy di setiap node,
dan Drain Count field digunakan untuk menyimpan informasi Drain Count.
Konsep drain count disini yaitu apabila suatu node memliki energy yang kurang dari threshold maka value dari path akan ber-increment bertambah 1, yang mana info tersebut akan disimpan di field
Drain Count. Jika suatu path memiliki drain count yang paling sedikit , makan path itulah yang akan dipilih untuk mengirim data. Apabila terdapat 2 atau lebih path yang memiliki drain count yang 
sama, maka path yang memiliki hop count yang paling sedikit yang akan dipilih.<br/>

<b> Modifikasi yang dilakukan </b></br>
1. Menambah field pada Packet Header AODV yaitu Remaining Energy, Drain Count, dan Record
2. Membuat fungsi untuk menambah counter pada field Drain Count
3. Fungsi sort untuk memilih drain count dan hop count yang paling sedikit yang akan dipilih
